#! /bin/bash

function usage() {
   echo "usage: $0 {WW_SOURCE_PATH}"
}

if [ $# -ne 1 ]; then
   echo 'wrong number of arguments'
   usage
   exit 1
fi

# Get the full path to the JNI directory
pushd ../ndk-project/jni &>/dev/null
   JNI_DIR="$(pwd)"
popd


WW_SOURCE="$1"
if [ -d "${WW_SOURCE}" ]; then
   echo 'Changing to WW_PROJECT directory...'
   pushd "${WW_SOURCE}" &>/dev/null
      # Get the full path to the WW_SOURCE directory
      WW_SOURCE=$(pwd)
   popd

   echo 'Setting up build environment...'
   for fn in $(ls "${WW_SOURCE}"); do
      ln -fs "${WW_SOURCE}/${fn}" "${JNI_DIR}"
   done

   echo 'Building Android NDK project...'
   pushd "${JNI_DIR}" &>/dev/null
      ndk-build
   popd &>/dev/null
else
   echo 'invalid directory specified'
   usage
   exit 1
fi
