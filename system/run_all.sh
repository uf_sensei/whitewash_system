#! /bin/bash

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${HOME}/local/lib
export LD_LIBRARY_PATH

function usage() {
  echo "usage: $0 {TYPE} {NUMNODES} {NUMCIR}"
}

if [ ! $# -eq 3 ]; then
  usage
  exit 1
fi

######################## CONFIGURATION SECTION ###################################

TYPE="$1"
NUMNODES="$2"
NUMCIR="$3"

if [ $TYPE -eq 0 ]; then
  PROGRAM="./evl"
elif [ $TYPE -eq 1 ]; then
  PROGRAM="./gen"
else
  usage
  echo "error: TYPE must be {0,1} for EVL or GEN respectively"
  exit 1
fi

SECURITY="80"
INPUT="dijk20inp.txt"
MODE=1
DATE="$(date +'%Y-%m-%d-%T')"

EVL_IP="127.0.0.1" # deathstar
EVL_PORT="13010"
GEN_PORT="13090"

######################### EXPERIMENTAL SECTION ####################################

function r10 () {
  # Run an experiment 10 times
  for i in {0..9};
  do
    OUTFILE="./results/WW_$(basename ${PROGRAM})_${DATE}_$(basename ${CIRCUIT})_${i}.result"
    mpirun  -n $NUMNODES $PROGRAM $SECURITY $NUMCIR $CIRCUIT $INPUT $EVL_IP $EVL_PORT $GEN_PORT $MODE | tee "${OUTFILE}"
  done
}

CIRCUIT="../../../circuits/PCF2Circ/dijkstra20OPT.pcf2"; r10
INPUT="dijk50inp.txt"
CIRCUIT="../../../circuits/PCF2Circ/dijkstra50OPT.pcf2"; r10
INPUT="dijk100inp.txt"
CIRCUIT="../../../circuits/PCF2Circ/dijkstra100OPT.pcf2"; r10
#INPUT="dijk200inp.txt"
#CIRCUIT="../../../circuits/PCF2Circ/dijkstra200OPT.pcf2"; r10
