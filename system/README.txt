#################################################
README.txt
Hank Carter
Whitewash project
3-25-2014
#################################################

############## UPDATES: ##############

--04-02-2014:  Added CCS.params to the mobile files necessary
for execution.  Also added renaming requirement for the mobile 
libraries.

--04-01-2014: Updated input parameters for ./gen and ./evl

--03-25-2014: original document

############## INSTRUCTIONS: ##############

1.  Install all requisite libraries for compiling Whitewash.  These are:

-Gnu Multiprecision Arithmetic Library (GMP) 
https://gmplib.org/
-Pairing-Based Cryptography Library (PBC)
http://crypto.stanford.edu/pbc/
-OpenSSL (typically installed by default)

We are using GMP 5.1.1 and PBC 0.5.13, but the latest versions should work as well.

2.  Install OpenMPI.  This should be possible using any Linux package manager.

3.  Navigate to the directory whitewash_system/system/ and type "make" to run the automated Makefile and compile the code.

4.  Repeat steps 1-2 on all machines if you are running the code between distinct servers.

5.  You are now ready to run all three parties from desktop machines.  To run the parties, run the following processes:

	NUMNODES: the number of nodes in the MPI cluster.  Must divide NUMCIR evenly
	SECURITY: The security parameter.  We use 80
	NUMCIR: The number of circuit copies to create.  Must be a multiple of NUMNODES.
	CIRCUIT: The name of the circuit file
	INPUT: The name of the input file
	EVL_IP: The IP address of the machine running the ./evl process.  The loopback address 127.0.0.1 can be used to run multiple processes on the same machine.
	EVL_PORT: The starting port number for the evaluator to attach to.
	GEN_IP: The IP address of the machine running ./gen.
	GEN_PORT: The starting port number for the generator to attach to.  Must be at least NUMNODES+1 less than or greater than EVL_PORT
	MODE: always use 1

	mpirun -n $NUMNODES ./evl $SECURITY $NUMCIR $CIRCUIT $INPUT $EVL_IP $EVL_PORT $GEN_PORT $MODE
	mpirun -n $NUMNODES ./gen $SECURITY $NUMCIR $CIRCUIT $INPUT $EVL_IP $EVL_PORT $GEN_PORT $MODE
	./phone $SECURITY $NUMCIR $CIRCUIT $INPUT $EVL_IP $EVL_PORT $GEN_IP $GEN_PORT $MODE

You will see logging data output as the execution takes place, along with a summary of timing statistics and output values once the computation is complete.

############## COMPILING AND RUNNING ON ANDROID: ##############

To compile and run the ./phone process for an android device.

1.  Install the Android SDK and NDK.  Specifically, you will be using the ADB tool to interact with the phone and the NDK to build the binaries.
http://developer.android.com/sdk/index.html

2.  Navigate to the directory whitewash_system/whitewash_phone/scripts/ and run the "build_phone.sh" script to compile the binary for the mobile device.

3.  Using ADB, set up the mobile device as follows:
-create a working directory on the mobile device.  We typically use /data/local/tmp/whitewash13/ because the phone doesn't have to be rooted to install and run 
binaries in this directory.
-Copy everything from the whitewash_system/whitewash_phone/ndk-project/libs/armeabi/ into the working directory on the phone
-Rename the libraries as follows: "libgmp.so" -> "libgmp.so.10", "libpbc.so" -> "libpbc.so.1"
-Copy the input files and circuit files to the working directory
-Copy the file "CCS.params" from the folder whitewash_system/whitewash_phone/scripts/ to the working directory.
-Set up the device to look for libraries locally by entering the following commands in an ADB shell:

	LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/data/local/tmp/whitewash13"
	export LD_LIBRARY_PATH

4.  Make sure the mobile device is connected to a wireless network and can ping the other machine(s) being used for computation.

5.  Run the following command after ./evl and ./gen are already running on the desktop machines:

	./wwmain $SECURITY $NUMCIR $CIRCUIT $INPUT $EVL_IP $EVL_PORT $GEN_IP $GEN_PORT $MODE
