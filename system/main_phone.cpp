#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <iostream>
#include <limits.h>
#include <netdb.h>
#include <pthread.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "Env.h"
#include "NetIO.h"
#include "Prng.h"

#define LINE_MAX _POSIX2_LINE_MAX

// Communication between evaluator and phone
#define EVL_PHONE_SEND(d)  Env::phone_to_evl()->write_bytes(d)
#define PHONE_EVL_RECV()   Env::phone_to_evl()->read_bytes()
#define PHONE_EVL_SEND(d)  Env::phone_to_evl()->write_bytes(d)
#define EVL_PHONE_RECV()   Env::phone_to_evl()->read_bytes()

// Communication between generator and phone
#define GEN_PHONE_SEND(d)  Env::phone_to_gen()->write_bytes(d)
#define PHONE_GEN_RECV()   Env::phone_to_gen()->read_bytes()
#define PHONE_GEN_SEND(d)  Env::phone_to_gen()->write_bytes(d)
#define GEN_PHONE_RECV()   Env::phone_to_gen()->read_bytes()

// Let's define some global variables 'cuz that's a good idea
std::vector<Bytes>           m_rnds;
std::vector<Bytes>           m_gen_inp_masks;
std::vector<Bytes>           m_gen_inp_decom;
std::vector<Bytes>           m_ot_out;
std::vector<Prng>				m_prngs;
Prng                    m_prng;
Bytes                   m_gen_inp;
uint32_t                m_gen_inp_cnt;
size_t                  m_ot_bit_cnt;
	
static byte MASK[8] = { 0xFF, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F};


uint32_t read_bob_length(const char * fname)
{
  uint32_t bob_in = 32;
  FILE * cnf;
  char line[LINE_MAX];
  cnf = fopen(fname, "r");
  // Bob's input is on the second line
  //
  // The input file format should be like this:
  //
  // 0xALICEINPUTSINHEX
  // 0x0000000000000000
  //
  // or
  //
  // 0x0000000000000000
  // 0xBOBINPUTSINHEX00
  fgets(line, LINE_MAX-1, cnf);
  fgets(line, LINE_MAX-1, cnf);
  fclose(cnf);
  bob_in = (strlen(line) - 1) * 4;
  return bob_in;
}


void init_network(EnvParams &params) {
	const int IP_SERVER_PORT = params.port_base;
	const int PORT = params.port_base + 2 + params.node_rank;
   const int PHONE_TO_EVL_PORT = params.port_base + 1;
   const int PHONE_TO_GEN_PORT = params.gen_port_base + 1;
	Bytes send, recv;

	// get local IP
	char hostname[1024];
	gethostname(hostname, 1024);
	struct hostent *host = gethostbyname(hostname);
	const std::string local_ip = inet_ntoa(*((struct in_addr *)host->h_addr_list[0]));

   // Create connections from phone to evaluator and generator
   std::cout << "PHONE (" << params.node_rank << ":" << local_ip << ") is trying to connect to EVL at port " << PHONE_TO_EVL_PORT << std::endl;
	params.phone_to_evl = new ClientSocket(params.ipserve_addr, PHONE_TO_EVL_PORT);
   std::cout << "PHONE (" << params.node_rank << ":" << local_ip << ") connected to EVL at port " << PHONE_TO_EVL_PORT << std::endl;

   std::cout << "PHONE (" << params.node_rank << ":" << local_ip << ") is trying to connect to GEN at port " << PHONE_TO_GEN_PORT << std::endl;
	params.phone_to_gen = new ClientSocket(params.gen_addr, PHONE_TO_GEN_PORT);
   std::cout << "PHONE (" << params.node_rank << ":" << local_ip << ") connected to GEN at port " << PHONE_TO_GEN_PORT << std::endl;
}


void init_environ(EnvParams &params)
{
	if (params.secu_param % 8 != 0 || params.secu_param > 128)
	{
      std::cerr << "security parameter k needs to be a multiple of 8 less than or equal to 128" << std::endl;
      exit(1);
	}

	if (params.stat_param % params.node_amnt != 0)
	{
      std::cerr << "statistical  parameter s needs to be a multiple of cluster size" << std::endl;
      exit(1);
	}

	// # of copies of a circuit each node is responsible for
	params.node_load = params.stat_param/params.node_amnt;

	Env::init(params);

	// synchronize claw-free collections
	ClawFree claw_free;
	claw_free.init();
	Bytes bufr(claw_free.size_in_bytes());

	Env::claw_free_from_bytes(bufr);
}


void init_private(EnvParams &params)
{
	static byte MASK[8] = { 0xFF, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F};

	std::ifstream private_file(params.private_file);
	std::string input;

	private_file >> input;
	private_file >> input;

	m_gen_inp.from_hex(input);


	if (!private_file.is_open())
	{
      std::cerr << "file open failed: " << params.private_file << std::endl;
      exit(1);
	}

	private_file.close();
}


void init_globals()
{
   m_rnds.resize(Env::node_load());
   m_gen_inp_masks.resize(Env::node_load());
	m_prngs.resize(2*Env::node_load());

	m_gen_inp_cnt = read_bob_length(Env::private_file());
	
   m_gen_inp.resize((m_gen_inp_cnt+7)/8);
	m_gen_inp.back() &= MASK[m_gen_inp_cnt%8];
}


void ot_random()
{
		Bytes send, recv;
		std::vector<Bytes> recv_chunks;

		Z r, s[2], t[2];
		G gr, hr, X[2], Y[2];

		m_ot_out.clear();
		m_ot_out.reserve(2*m_ot_bit_cnt); // the receiver only uses half of it

		for (size_t bix = 0; bix < m_ot_bit_cnt; bix++)
		{
			//start = MPI_Wtime();
				Y[0].random(); Y[1].random(); // K[0], K[1] sampled at random

				m_ot_out.push_back(Y[0].to_bytes().hash(Env::k()));
				m_ot_out.push_back(Y[1].to_bytes().hash(Env::k()));
				
				send += Y[0].to_bytes();
				send += Y[1].to_bytes();
			//m_timer_phone += MPI_Wtime() - start;
		}

		//start = MPI_Wtime();
         PHONE_GEN_SEND(send);
		//m_timer_com += MPI_Wtime() - start;

		assert(m_ot_out.size() == 2*m_ot_bit_cnt);
}


void cut_and_choose2_ot()
{
	m_ot_bit_cnt = Env::node_load();

 //std::cout <<"phone1.0\n";

   //ot_init();
   ot_random();


// std::cout <<"phone1.1\n";

   //start = MPI_Wtime();
   	m_prngs.resize(2*Env::node_load());
   	for (size_t ix = 0; ix < m_prngs.size(); ix++)
   	{
   		m_prngs[ix].srand(m_ot_out[ix]);
   	}

 //std::cout <<"phone1.2\n";
   //m_timer_phone += MPI_Wtime() - start;
}


void cut_and_choose2_precomputation()
{
   Bytes rnds_bufr, gen_inp_masks_bufr;

   for (size_t ix = 0; ix < m_rnds.size(); ix++)
   {
      //start = MPI_Wtime();
         m_rnds[ix] = m_prng.rand(Env::k());
         rnds_bufr += m_rnds[ix];
         m_gen_inp_masks[ix] = m_prng.rand(m_gen_inp_cnt);
         gen_inp_masks_bufr += m_gen_inp_masks[ix];
   }

   //start = MPI_Wtime();
   	PHONE_GEN_SEND(rnds_bufr);
      PHONE_GEN_SEND(gen_inp_masks_bufr);
   //m_timer_com += MPI_Wtime() - start;
}


void cut_and_choose2_evl_circuit(size_t ix, Bytes &mask_bufr, Bytes &decom_bufr)
{
   Bytes bufr;
   std::vector<Bytes> bufr_chunks;

   // send masked gen inp
   //start = MPI_Wtime();
   	bufr = m_gen_inp_masks[ix] ^ m_gen_inp;
   	bufr ^= m_prngs[2*ix+0].rand(bufr.size()*8); // encrypt message
      mask_bufr += bufr;
   //m_timer_phone += MPI_Wtime() - start;
   
   // Why? 'Cuz need to keep state N'Sync
	//start = MPI_Wtime();
	   m_prngs[2*ix+0].rand(2*Env::k()); // encrypt message
	//m_timer_phone += MPI_Wtime() - start;
   // bye bye bye

   bufr.clear();
	for (size_t jx = 0; jx < m_gen_inp_cnt; jx++)
	{
      //start = MPI_Wtime();
      	byte bit = m_gen_inp.get_ith_bit(jx) ^ m_gen_inp_masks[ix].get_ith_bit(jx);
      //m_timer_phone += MPI_Wtime() - start;
         
      //start = MPI_Wtime();
         //bufr_chunks = bufr.split(bufr.size() / 2); // b/c we received two values
      	//bufr = bufr_chunks[bit];
         bufr = m_gen_inp_decom[2*m_gen_inp_cnt*ix + jx*2 + bit];
      	bufr ^= m_prngs[2*ix+0].rand(bufr.size()*8); // encrypt message
         decom_bufr += bufr;
      //m_timer_phone += MPI_Wtime() - start;
      
      //start = MPI_Wtime();
      	//PHONE_EVL_SEND(bufr);
      //m_timer_com += MPI_Wtime() - start;
   }
}


void proc_gen_out()
{
   // Store the output for now
   // TODO: Should print it or something later
   Bytes m_gen_out;
   m_gen_out = PHONE_EVL_RECV();

	std::cout <<"PHONE OUTPUT: "<<m_gen_out.to_hex()<<"\n";
}


void cut_and_choose2()
{
   Bytes bufr, mask_bufr, decom_bufr;

	// std::cout <<"phone2.0\n";

   cut_and_choose2_ot();

	 //std::cout <<"phone2.1\n";

   cut_and_choose2_precomputation();

// std::cout <<"phone2.2\n";

   bufr = PHONE_GEN_RECV();
   
  // std::cout << "phone2.25 "<<m_gen_inp_cnt <<"\n";

   m_gen_inp_decom = bufr.split(bufr.size() / (Env::s() * m_gen_inp_cnt * 2));

 //std::cout <<"phone2.3\n";

   bufr.clear();
	for (size_t ix = 0; ix < m_rnds.size(); ix++)
	{
		cut_and_choose2_evl_circuit(ix, mask_bufr, decom_bufr);
	}

 //std::cout <<"phone2.4\n";

   PHONE_EVL_SEND(mask_bufr);
   PHONE_EVL_SEND(decom_bufr);
}


void circuit_evaluate()
{
   Bytes bufr;

   bufr = PHONE_GEN_RECV();
   if (bufr.size() == 1)
      proc_gen_out();
}


int main(int argc, char **argv)
{
	if (argc < 10)
	{
		std::cout << "Usage:" << std::endl
			<< "\tbetteryao [secu_param] [stat_param] [pcf_file] [input_file] [ip_server] [port_base] [gen_server] [gen_port_base] [mode]" << std::endl
			<< std::endl
			<< "    [secu_param]: multiple of 8 but 128 at most" << std::endl
			<< "    [stat_param]: multiple of the cluster size" << std::endl
			<< "      [pcf_file]: from the PCF compiler" << std::endl
			<< "     [ip_server]: the IP (not domain name) of the IP exchanger" << std::endl
			<< "     [port_base]: for the \"IP address in use\" hassles" << std::endl
         << "    [gen_server]: the IP (not domain name) of the GEN server" << std::endl
         << " [gen_port_base]: the start port number for the GEN server" << std::endl
			<< "          [mode]: 0=>honest-but-curious, 4=>malicious" << std::endl
			<< std::endl;
		exit(EXIT_FAILURE);
	}

	EnvParams params;

	params.secu_param   = atoi(argv[1]);
	params.stat_param   = atoi(argv[2]);

	//params.circuit_file = argv[3];
	params.pcf_file      = argv[3];
	params.private_file  = argv[4];
	params.ipserve_addr  = argv[5];
	params.port_base     = atoi(argv[6]);
   params.gen_addr      = argv[7];
	params.gen_port_base = atoi(argv[8]);

   // Always set this to 1 on the phone, gets set by MPI elsewhere
   params.node_amnt = 1;

   // Initialize environment parameters (copied from YaoBase.cpp) and globals
   init_network(params);
   init_environ(params);
   init_private(params);
   init_globals();


   //std::cout <<"phone1\n";

   // Begin working on protocol
   cut_and_choose2();
  //std::cout <<"phone2\n";  
 circuit_evaluate();
// std::cout <<"phone3\n";

   return 0;
}
