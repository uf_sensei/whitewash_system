#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "pcflib.h"

uint32_t * (*input_callback)(struct PCFState *, struct PCFGate *);
void (*output_callback)(struct PCFState *, struct PCFGate *);

uint64_t gate_count = 0;
uint64_t non_xor_count = 0;

uint64_t get_gate_count ()
{
  return gate_count;
}

uint64_t get_non_xor_count()
{
  return non_xor_count;
}

void * copykey(void * key)
{
  uint32_t * orig = (uint32_t*)key;
  if(orig != 0)
    {
      uint32_t * copy = (uint32_t*)malloc(sizeof(uint32_t));
      *copy = *orig;
      assert((*copy == 0) || (*copy == 1));
      return copy;
    }
  else return 0;
}

void deletekey(void *key)
{
  if(key != 0)
    free(key);
}

uint32_t o_key;

void * m_callback(struct PCFState * st, struct PCFGate * gate)
{
  if(gate->tag == TAG_INTERNAL)
    {
      uint8_t bits[4];
      int8_t i = 0;
      uint8_t tab = gate->truth_table;
      for(i = 3; i >= 0; i--)
        {
          bits[i] = tab & 0x01;
          tab = tab >> 1;
        }

      uint32_t * key = &o_key;//(uint32_t*)malloc(sizeof(uint32_t));
      uint32_t wire1val, wire2val;

      assert(st->wires[gate->wire1] != 0);
      assert(st->wires[gate->wire2] != 0);
      wire1val = *((uint32_t*)st->wires[gate->wire1]->keydata);
      wire2val = *((uint32_t*)st->wires[gate->wire2]->keydata);
        
      *key = bits[(wire1val & 0x01) + 2*(wire2val & 0x01)];

      gate_count++;
      if(gate->truth_table != 6)
        non_xor_count++;

      return key;
    }
  else if((gate->tag == TAG_INPUT_A) || (gate->tag == TAG_INPUT_B))
    {
      return input_callback(st, gate);
    }
  else
    {
      o_key = 0;
      output_callback(st, gate);
      return &o_key;
    }
}

void run_pcf(const char * fname, uint32_t * (*ic)(struct PCFState *, struct PCFGate *), void (*oc)(struct PCFState *, struct PCFGate *), void (*final_print)(struct PCFState *))
{
  uint32_t *keys = malloc(sizeof(uint32_t)*2);
  keys[0] = 0;
  keys[1] = 1;

  input_callback = ic;
  output_callback = oc;

  struct PCFState * st = load_pcf_file(fname, &keys[0], &keys[1], copykey);
  struct PCFGate * g;

  set_callback(st, m_callback);
  set_key_copy_function(st, copykey);
  set_key_delete_function(st, deletekey);

  g = get_next_gate(st);

  while(g != 0)
    {
      g = get_next_gate(st);
    }

  final_print(st);
  gate_count = 0;
  non_xor_count = 0;

}
