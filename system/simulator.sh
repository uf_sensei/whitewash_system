#!/usr/bin/sbcl --script

(declaim (sb-ext:muffle-conditions sb-ext:compiler-note))
(load "simulator.lisp")

(handler-bind ((sb-sys:interactive-interrupt #'(lambda (c) 
                                                 (declare (ignore c))
                                                 (format *error-output* "Received interrupt, quitting.~%")
                                                 (quit)))
               )
  (toplevel)
  )
