(use-package :sb-alien)

(declaim (optimize (speed 3) (safety 0) (debug 0) (compilation-speed 0)))

(sb-alien:load-shared-object "./simulator.so")

(define-alien-type PCFGate 
    (struct PCFGate
            (wire1 (sb-alien:unsigned 32))
            (wire2 (sb-alien:unsigned 32))
            (reswire (sb-alien:unsigned 32))
            (truth_table (sb-alien:unsigned 8))
            (tag (sb-alien:unsigned 32))
            )
  )

(sb-alien:define-alien-routine malloc (* t) (size int))
(sb-alien:define-alien-routine free void (ptr (* t)))
(sb-alien:define-alien-routine run-pcf void (fname c-string) 
                               (ic (function (* (unsigned 32)) (* t) (* PCFGate)))
                               (oc (function void (* t) (* PCFGate)))
                               (pf (function void (* t)))
                               )
(sb-alien:define-alien-routine get-wire-key (* t) (state (* t)) (idx (unsigned 32)))
(sb-alien:define-alien-routine get-gate-count (unsigned 64))
(sb-alien:define-alien-routine get-non-xor-count (unsigned 64))

(define-alien-type tags
    (enum tags
          (tag-int 0)
          (tag-inp-a 2)
          (tag-inp-b 4)
          (tag-out-a 6)
          (tag-out-b 8)))

(defvar alice-inputs nil)
(defvar bob-inputs nil)
(defvar alice-outputs nil)
(defvar bob-outputs nil)

(defvar alice-input-cnt 0)
(defvar bob-input-cnt 0)

(defvar ret-key (cast (malloc (alien-size (unsigned 32) :bytes)) (* (unsigned 32))))

(sb-alien::define-alien-callback input-gate-callback (* (unsigned 32)) ((state (* t)) (gate (* PCFGate)))
  (declare (ignore state))
  (let ((wire1 (slot gate 'wire1))
        )
    (if (= (slot gate 'tag) 2)
        (progn
          (incf alice-input-cnt)
          (setf (deref ret-key) (if (< wire1 (length alice-inputs)) 
                                    (nth wire1 alice-inputs) 
                                    0)
                )
          )
        (progn
          (incf bob-input-cnt)
          (setf (deref ret-key) (if (< wire1 (length bob-inputs)) 
                                    (nth wire1 bob-inputs) 
                                    0)
                )
          )
        )
    ret-key
    )
  )

(sb-alien::define-alien-callback output-gate-callback void ((state (* t)) (gate (* PCFGate)))
  (let ((wire1 (deref (cast (get-wire-key state (slot gate 'wire1)) (* (unsigned 32)))))
        )
    (if (= (slot gate 'tag) 6)
        (setf alice-outputs (cons wire1 alice-outputs))
        (setf bob-outputs (cons wire1 bob-outputs))
        )
    )
  )

(sb-alien::define-alien-callback final-print void ((state (* t)))
  (declare (ignore state))
  (let ((*print-radix* t)
        (*print-base* 16))
    (format t "~&Alice output: ~64,'0X~%" (list-to-int (reverse alice-outputs)))
    (format t "~&Bob output:   ~64,'0X~%" (list-to-int (reverse bob-outputs)))
    )
  (format t "~&Non-xor gates: ~D~%" (get-non-xor-count))
  (format t "~&Total gates: ~D~%" (get-gate-count))

  (format t "~&Alice input bits: ~D~%" alice-input-cnt)
  (format t "~&Bob input bits: ~D~%" bob-input-cnt)
  )

(defun int-to-list (n bits)
  (if (zerop bits)
      nil
      (cons (mod n 2) (int-to-list (floor (/ n 2)) (1- bits)))
      )
  )

(defun list-to-int (lst)
  (if (null lst)
      0
      (+ (first lst) (* 2 (list-to-int (rest lst))))
      )
  )

(defun test-alien (fname inpt)
  (setf alice-outputs nil)
  (setf bob-outputs nil)
  (with-open-file (inputs inpt :direction :input)
    (let ((*read-base* 16)
          (*print-radix* t)
          (*print-base* 16)
          (alice-in (parse-integer (read-line inputs) :radix 16))
          (bob-in (parse-integer (read-line inputs) :radix 16))
          )
      (format t "Alice input:  ~32,'0X~%" alice-in)
      (format t "Bob input:    ~32,'0X~%" bob-in)

      (setf alice-inputs (int-to-list alice-in 256))
      (setf bob-inputs (int-to-list bob-in 256))
      )
    )

  (assert (and (= (length alice-inputs) 256)
               (= (length bob-inputs) 256)))

  (format t "Simulating~%")    
  (sb-alien::with-pinned-objects (alice-inputs bob-inputs alice-outputs bob-outputs ret-key)
    (time (run-pcf fname input-gate-callback output-gate-callback final-print))
    )

  (setf alice-input-cnt 0)
  (setf bob-input-cnt 0)
  )

(defun toplevel ()
  (test-alien (second sb-ext:*posix-argv*) (third sb-ext:*posix-argv*))
  (setf alice-input-cnt 0)
  (setf bob-input-cnt 0)
  )